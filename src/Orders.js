import React from 'react';
import './App.css';
import Typography from '@material-ui/core/Typography';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';

function Orders() {
  return (
    <div className="Order-summary">
      <Typography variant="h5" component="h5" gutterBottom>
        Order Summary
      </Typography>
      <ul class="summaryList">
        <li>
          <div class="sl-imageTitle">
            <img src={require('./seo.png')} />
            <h3>SEOTesterOnline</h3>   
          </div> 
          <div class="sl-select">
            <select>'
              <option>Gold Member</option>
              <option>Silver Member</option>
            </select>  
          </div>        
          <div class="sl-itemnumber">
            <TextField
              id="outlined-bare"
              defaultValue="1"
              margin="normal"
              variant="outlined"
              inputProps={{ 'aria-label': '1' }}
            />  
          </div>   
          <div class="sl-price">$59</div>  
        </li>
        <li>
          <div class="sl-imageTitle">
           <img src={require('./seo.png')} />
            <h3>SendPilot</h3>   
          </div> 
          <div class="sl-select">
            <select>'
              <option>Gold Member</option>
              <option>Silver Member</option>
            </select>  
          </div>        
          <div class="sl-itemnumber">
            <TextField
              id="outlined-bare"
              defaultValue="1"
              margin="normal"
              variant="outlined"
              inputProps={{ 'aria-label': '1' }}
            />  
          </div>   
          <div class="sl-price">$49</div>  
        </li>
        <li>
          <div class="sl-imageTitle">
            <img src={require('./seo.png')} />
            <h3>Personizly</h3>   
          </div> 
          <div class="sl-select">
            <select>'
              <option>Gold Member</option>
              <option>Silver Member</option>
            </select>  
          </div>        
          <div class="sl-itemnumber">
            <TextField
              id="outlined-bare"
              defaultValue="1"
              margin="normal"
              variant="outlined"
              inputProps={{ 'aria-label': '1' }}
            />  
          </div>   
          <div class="sl-price">$79</div>  
        </li>
      </ul>
      <div class="sl-Totalbox">
        <h2>Personizly <span class="sl-totalPrice">$789 One Time</span></h2> 
       <FormControlLabel control={<Checkbox value="checkedC" />} label="I Agree to all the PitchGround Terms & Conditions" />
        <div class="sl-Secure">100% Safe and Secure</div>
      </div>
    </div>
  );
}

export default Orders;
