import React from 'react';
import logo from './logo.svg';
import './App.css';

import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';

import Payment from "./Payment";
import Orders from "./Orders";

function App() {
  return (
  <div class="Mainbox">
  <Box component="span">
    <Container fixed>      
    <Grid container>
      <Grid item xs={4}>
        <Payment />
      </Grid>
      <Grid item xs={8}>
        <Orders />
      </Grid>
    </Grid>        
    </Container>
  </Box>
  </div>
  );
}

export default App;
